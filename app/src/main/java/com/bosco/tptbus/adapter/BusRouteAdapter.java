package com.bosco.tptbus.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bosco.tptbus.R;
import com.bosco.tptbus.bean.BusRouteData;
import com.bosco.tptbus.utils.DateAndTimeCalculation;

import java.util.List;

public class BusRouteAdapter extends RecyclerView.Adapter<BusRouteAdapter.MyViewHolder> {

    private List<BusRouteData> dataList;

    public BusRouteAdapter( List<BusRouteData> dataList) {
        this.dataList = dataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_bus_route_items,viewGroup,false);

        MyViewHolder myViewHoder = new MyViewHolder(view);

        return myViewHoder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHoder, int i) {

        BusRouteData routeData  = dataList.get(i);

        myViewHoder.textViewPlaceName.setText(routeData.getPlaceName());
        myViewHoder.textViewTickerPrice.setText("RS "+routeData.getTicketPrice());

        String time = DateAndTimeCalculation.timeStampToTime(routeData.getReachingTime());
        myViewHoder.textViewReacheingTime.setText(time);


    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView textViewPlaceName;
        TextView textViewTickerPrice;
        TextView textViewReacheingTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            textViewPlaceName = itemView.findViewById(R.id.textViewPlaceName);
            textViewTickerPrice = itemView.findViewById(R.id.textViewTeicketPrice);
            textViewReacheingTime = itemView.findViewById(R.id.textViewReachingTime);

        }
    }
}
