package com.bosco.tptbus.adapter;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bosco.tptbus.R;
import com.bosco.tptbus.bean.HomeData;
import com.bosco.tptbus.interfaces.NameKeys;
import com.bosco.tptbus.utils.DateAndTimeCalculation;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeAdapater extends RecyclerView.Adapter<HomeAdapater.MyViewHolder> {

    private Context context;
    private List<HomeData> homeDataList;

    public HomeAdapater(Context context, List<HomeData> homeDataList) {
        this.context = context;
        this.homeDataList = homeDataList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.layout_home_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHoder, int i) {

        final HomeData homeData = homeDataList.get(i);

//        myViewHoder.imageView.setImageResource(homeData.getBusImage());
        myViewHoder.textViewBusName.setText(homeData.getBusName());
        myViewHoder.textViewPlaceName.setText(homeData.getPlaceName());

        Picasso.with(context)
                .load(homeData.getImgUrl())
                .placeholder(android.R.color.darker_gray)
                .into(myViewHoder.imageView);


        String time = DateAndTimeCalculation.timeStampToTime(homeData.getBusTime());
//        myViewHoder.textViewBusTime.setText(""+homeData.getBusTime());
        myViewHoder.textViewBusTime.setText("" + time);


        myViewHoder.itemView.setOnClickListener(v -> {
            Log.d("SELECTED_BUS", "" + homeData.getBusName());
            Log.d("SELECTED_BUS_DATE", "" + homeData.getBusTime());

            Bundle bundle = new Bundle();
            bundle.putString(NameKeys.BundleKyes.BUS_NAME_KEY, homeData.getBusName());
            bundle.putString(NameKeys.BundleKyes.IMG_URL_KEY, homeData.getImgUrl());
            bundle.putString(NameKeys.BundleKyes.DESTINATION, homeData.getPlaceName());
            bundle.putString(NameKeys.BundleKyes.BUS_TIME, DateAndTimeCalculation.timeStampToCurrentDate(homeData.getBusTime()));

            Navigation.findNavController(v).navigate(R.id.action_homeFragment_to_busRouteFragment, bundle);

        });
    }

    @Override
    public int getItemCount() {
        return homeDataList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView textViewBusName;
        TextView textViewPlaceName;
        TextView textViewBusTime;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.imageViewItemBusImage);
            textViewBusName = itemView.findViewById(R.id.textViewItmeBusName);
            textViewPlaceName = itemView.findViewById(R.id.textViewItemPlaceName);
            textViewBusTime = itemView.findViewById(R.id.textViewItemBusTime);
        }
    }
}
