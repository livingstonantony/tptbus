package com.bosco.tptbus.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.bosco.tptbus.R;
import com.bosco.tptbus.interfaces.ClickEvent;
import com.bosco.tptbus.ui.fragment.HomeFragment;

import java.util.ArrayList;
import java.util.List;

public class SingleTextAdapter extends RecyclerView.Adapter<SingleTextAdapter.MyViewHolder> {

    private Context context;
    private List<String> dataList;
    private List<String> list;
    private ClickEvent clickEvent;

    public SingleTextAdapter(HomeFragment homeFragment, Context context, List<String> dataList) {
        this.context = context;
        this.dataList = dataList;
        this.list = new ArrayList<>();
        this.list.addAll(dataList);
        this.clickEvent = (ClickEvent) homeFragment;
        this.dataList.clear();
    }

    @NonNull
    @Override
    public SingleTextAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_sigle_text, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleTextAdapter.MyViewHolder holder, final int position) {


        holder.textView.setText(dataList.get(position));

        holder.itemView.setOnClickListener(v -> {

            String s = dataList.get(position);

            Log.d("DATA", "" + s);

            clickEvent.click(s);
        });
    }

    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {


        TextView textView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            textView = itemView.findViewById(R.id.textView);
        }
    }

    public void filter(String s) {

        s = s.toLowerCase();

        dataList.clear();

        if (s.length() == 0) {
//            dataList.addAll(list);
        } else {
            for (String s1 : list) {

                if (s1.toLowerCase().contains(s.toLowerCase())) {

                  /*  String hilight = Utils.highlightText(s,places.getBusName()).toString();

                    places.setBusName(hilight);*/

                    dataList.add(s1);
                }
            }
        }

        notifyDataSetChanged();
    }
}
