package com.bosco.tptbus.bean;

import com.google.firebase.Timestamp;

public class BusRouteData {

    private String placeName;
    private int ticketPrice;
    private Timestamp reachingTime;

    public BusRouteData(String placeName, int ticketPrice, Timestamp reachingTime) {
        this.placeName = placeName;
        this.ticketPrice = ticketPrice;
        this.reachingTime = reachingTime;
    }

    public String getPlaceName() {
        return placeName;
    }


    public int getTicketPrice() {
        return ticketPrice;
    }


    public Timestamp getReachingTime() {
        return reachingTime;
    }

}
