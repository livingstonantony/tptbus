package com.bosco.tptbus.bean;

import com.google.firebase.Timestamp;

public class HomeData {

    private String busName;
    private String placeName;
    private Timestamp busTime;
    private String imgUrl;


    public HomeData(String busName, String placeName, Timestamp busTime, String imgUrl) {
        this.busName = busName;
        this.placeName = placeName;
        this.busTime = busTime;
        this.imgUrl = imgUrl;
    }


    public String getBusName() {
        return busName;
    }


    public String getPlaceName() {
        return placeName;
    }


    public Timestamp getBusTime() {
        return busTime;
    }


    public String getImgUrl() {
        return imgUrl;
    }

}
