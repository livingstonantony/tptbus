package com.bosco.tptbus.bean;

import com.google.firebase.Timestamp;

import java.util.List;

public class HomeDataAll {

    private String name;
    private String IMAGE_URL;
    private List<Timestamp> dateList;
    private List<String> placeList;


    public HomeDataAll(String name, String IMAGE_URL, List<Timestamp> dateList, List<String> placeList) {
        this.name = name;
        this.IMAGE_URL = IMAGE_URL;
        this.dateList = dateList;
        this.placeList = placeList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIMAGE_URL() {
        return IMAGE_URL;
    }


    public List<Timestamp> getDateList() {
        return dateList;
    }


    public List<String> getPlaceList() {
        return placeList;
    }

}
