package com.bosco.tptbus.interfaces;

public interface NameKeys {

    interface HomeDataKeys{
        String BUS_NAME_KEY = "bus_name";
        String IMG_URL = "img_url";
        String TIME = "time";
        String PLACES = "places";
    }

    interface BundleKyes{
        String BUS_NAME_KEY = "busName";
        String IMG_URL_KEY = "imgURL";
        String BUS_TIME = "butTime";
        String DESTINATION = "destination";

        String BUS_NAME  = "busName";
        String BUS_NUMBER = "busNumber";
        String DRIVER = "driver";
        String CONDUCTOR = "conductor";
        String OWNER = "owner";
        String OWNER_NUMBER = "ownerNumber";
    }

    interface BusRouteKesy{
        String COLLECTION_KEY = "buses";
        String KEY = "bus_routes";
        String PLACE = "place";
        String TIME = "time";
        String PRICE = "price";

        String BUS_NAME  = "bus_name";
        String BUS_NUMBER = "bus_number";
        String DRIVER = "driver";
        String CONDUCTOR = "conductor";
        String OWNER = "owner";
        String OWNER_NUMBER = "owner_number";
    }

    interface  FeedBack{

        String COLLECTION = "feedback";
        String DOCUMENT = "about_app";

        String DEVICE_MODEL = "mobile_model";
        String TITLE = "title";
        String DESCRIPTION = "description";
    }
}
