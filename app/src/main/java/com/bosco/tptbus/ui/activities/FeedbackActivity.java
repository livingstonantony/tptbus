package com.bosco.tptbus.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.bosco.tptbus.R;
import com.bosco.tptbus.interfaces.NameKeys;
import com.bosco.tptbus.utils.Utils;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;
import java.util.Map;

public class FeedbackActivity extends AppCompatActivity {

    private FirebaseFirestore db;

    private EditText editTextTitle;
    private EditText editTextDescription;
    private Button button;

    private Map<String, Object> feedback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Feedback");

        db = FirebaseFirestore.getInstance();


        editTextTitle = findViewById(R.id.editTextTitle);
        editTextDescription = findViewById(R.id.editTextDescription);

        button = findViewById(R.id.buttonSubmit);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setData();

            }
        });
    }

    private void setData() {
        String deviceModel = Utils.getDeviceName();
        String title = editTextTitle.getText().toString();
        String descritpion = editTextDescription.getText().toString();

        if (title.equals("")) {
            editTextTitle.setError("Enter Title");
        } else if (descritpion.equals("")) {
            editTextDescription.setError("Enter Description");
        } else {
            editTextTitle.getText().clear();
            editTextDescription.getText().clear();

            Utils.showProgressView(getWindow(), findViewById(R.id.progressView), true);

            feedback = new HashMap<>();
            feedback.put(NameKeys.FeedBack.DEVICE_MODEL, deviceModel);
            feedback.put(NameKeys.FeedBack.TITLE, title);
            feedback.put(NameKeys.FeedBack.DESCRIPTION, descritpion);

            sendFeeback();
        }


    }

    private void sendFeeback() {
        db.collection(NameKeys.FeedBack.COLLECTION)
                .add(feedback)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference aVoid) {
                        Log.d("RESULT", "DocumentSnapshot successfully written!");
                        Utils.showProgressView(getWindow(), findViewById(R.id.progressView), false);
                        Toast.makeText(FeedbackActivity.this, "Thank you for the feedback..!", Toast.LENGTH_SHORT).show();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("ERROR", "Error writing document", e);
                        Utils.showProgressView(getWindow(), findViewById(R.id.progressView), false);
                        Toast.makeText(FeedbackActivity.this, "Feedback not sent..!", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
