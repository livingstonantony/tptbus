package com.bosco.tptbus.ui.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bosco.tptbus.R;
import com.bosco.tptbus.interfaces.NameKeys;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusDetialsFragment extends Fragment {


    public BusDetialsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bus_detials, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle("Bus Detail");

       TextView textViewBusName = view.findViewById(R.id.textViewBusName);
       TextView textViewBusNumber = view.findViewById(R.id.textViewBusNumber);
       TextView textViewDriver = view.findViewById(R.id.textViewDriver);
       TextView textViewConductor = view.findViewById(R.id.textViewConductor);
       TextView textViewOwner = view.findViewById(R.id.textViewOwner);
       TextView textViewOwnerNumber = view.findViewById(R.id.textViewOwnerNumber);

        String busName = getArguments().getString(NameKeys.BundleKyes.BUS_NAME);
        String busNumber = getArguments().getString(NameKeys.BundleKyes.BUS_NUMBER);
        String driver = getArguments().getString(NameKeys.BundleKyes.DRIVER);
        String conductor = getArguments().getString(NameKeys.BundleKyes.CONDUCTOR);
        String owner = getArguments().getString(NameKeys.BundleKyes.OWNER);
        String ownerNumber = getArguments().getString(NameKeys.BundleKyes.OWNER_NUMBER);

        textViewBusName.setText(busName);
        textViewBusNumber.setText(busNumber);
        textViewDriver.setText(driver);
        textViewConductor.setText(conductor);
        textViewOwner.setText(owner);
        textViewOwnerNumber.setText(ownerNumber);
    }
}
