package com.bosco.tptbus.ui.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bosco.tptbus.R;
import com.bosco.tptbus.adapter.BusRouteAdapter;
import com.bosco.tptbus.bean.BusRouteData;
import com.bosco.tptbus.interfaces.NameKeys;
import com.bosco.tptbus.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class BusRouteFragment extends Fragment {


    private List<BusRouteData> busRouteData;
    private BusRouteAdapter busRouteAdapater;
    private RecyclerView recyclerView;

    private FirebaseFirestore db;
    private ImageView imageView;

    private String busName = "";
    private String busNumber = "";
    private String driver = "";
    private String conductor = "";
    private String owner = "";
    private String ownerNumber = "";
    private String destination = "";

    public BusRouteFragment() {
        // Required empty public constructor
    }

    String BUS_TIME = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_bus_route, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        Utils.showProgressView(getActivity().getWindow(), getActivity().findViewById(R.id.progressView), true);

        db = FirebaseFirestore.getInstance();

        String DOCUMENT = getArguments().getString(NameKeys.BundleKyes.BUS_NAME_KEY);
        // set bus name as tittle of the action bar
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(DOCUMENT);
        DOCUMENT = DOCUMENT.toLowerCase();
        String IMG_URL = getArguments().getString(NameKeys.BundleKyes.IMG_URL_KEY);
        BUS_TIME = getArguments().getString(NameKeys.BundleKyes.BUS_TIME);
        destination = getArguments().getString(NameKeys.BundleKyes.DESTINATION);


        imageView = getView().findViewById(R.id.imageView);


        busRouteData = new ArrayList<>();
//        busRouteData.addAll(Utils.prepareDummnyRouteData());

        recyclerView = getView().findViewById(R.id.recyclerViewBusRoute);
        busRouteAdapater = new BusRouteAdapter( busRouteData);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(busRouteAdapater);

        getBusRouteData(NameKeys.BusRouteKesy.COLLECTION_KEY, DOCUMENT, NameKeys.BusRouteKesy.KEY);

        Picasso.with(getContext())
                .load(IMG_URL)
                .placeholder(android.R.color.darker_gray)
                .into(imageView);


        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Bundle bundle = new Bundle();

                bundle.putString(NameKeys.BundleKyes.BUS_NAME, busName);
                bundle.putString(NameKeys.BundleKyes.BUS_NUMBER, busNumber);
                bundle.putString(NameKeys.BundleKyes.DRIVER, driver);
                bundle.putString(NameKeys.BundleKyes.CONDUCTOR, conductor);
                bundle.putString(NameKeys.BundleKyes.OWNER, owner);
                bundle.putString(NameKeys.BundleKyes.OWNER_NUMBER, ownerNumber);

                Navigation.findNavController(v).navigate(R.id.action_busRouteFragment_to_busDetialsFragment, bundle);
            }
        });
    }

    private void getBusRouteData(String collection, final String document, final String key) {
        DocumentReference user = db.collection(collection).document(document);

        user.get().addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                DocumentSnapshot doc = task.getResult();
                StringBuilder fields = new StringBuilder("");
                fields.append("\nBUSES: ").append(doc.get(key));

                Log.d("BUS_ROUTE", "" + fields);

                List list = (List) doc.get(key);

                busName = (String) doc.get(NameKeys.BusRouteKesy.BUS_NAME);
                busNumber = (String) doc.get(NameKeys.BusRouteKesy.BUS_NUMBER);
                driver = (String) doc.get(NameKeys.BusRouteKesy.DRIVER);
                conductor = (String) doc.get(NameKeys.BusRouteKesy.CONDUCTOR);
                owner = (String) doc.get(NameKeys.BusRouteKesy.OWNER);
                ownerNumber = (String) doc.get(NameKeys.BusRouteKesy.OWNER_NUMBER);

                if (list != null) {

                    busRouteData.addAll(Utils.preateBusRouteData(list, BUS_TIME, destination));
                    busRouteAdapater.notifyDataSetChanged();
                } else {
                    Toast.makeText(getContext(), "Data Not Found..!", Toast.LENGTH_SHORT).show();
                }

                Utils.showProgressView(getActivity().getWindow(), getActivity().findViewById(R.id.progressView), false);

            }
        })
                .addOnFailureListener(e -> {
                    Log.d("ERROR", "" + e);
                    Utils.showProgressView(getActivity().getWindow(), getActivity().findViewById(R.id.progressView), false);

                });
    }


}
