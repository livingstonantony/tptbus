package com.bosco.tptbus.ui.fragment;

import android.app.TimePickerDialog;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.bosco.tptbus.R;
import com.bosco.tptbus.adapter.HomeAdapater;
import com.bosco.tptbus.bean.HomeData;
import com.bosco.tptbus.bean.HomeDataAll;
import com.bosco.tptbus.adapter.SingleTextAdapter;
import com.bosco.tptbus.interfaces.ClickEvent;
import com.bosco.tptbus.utils.DateAndTimeCalculation;
import com.bosco.tptbus.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 */
public class HomeFragment extends Fragment implements ClickEvent {


    private HomeAdapater homeAdapater;
    private RecyclerView recyclerView;

    private List<HomeData> homeDataList;
    private List<HomeData> homeDataList1;
    private List<HomeDataAll> homeDataAlls;

    private FirebaseFirestore db;

    private SingleTextAdapter singleTextAdapter;

    private RecyclerView recyclerViewSingleText;
    private List<String> stringList;

    private TextView editTextTime;
    private EditText editTextPlace;

    private String selectedDate = "";

    private TextView textViewIsBusAvailable;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(R.string.app_name);
        db = FirebaseFirestore.getInstance();

        homeDataList = new ArrayList<>();
        homeDataList1 = new ArrayList<>();
        stringList = new ArrayList<>();

        homeDataAlls = new ArrayList<>();

        recyclerViewSingleText = view.findViewById(R.id.recyclerViewSingleText);

        editTextTime = view.findViewById(R.id.editTextTime);
        editTextPlace = view.findViewById(R.id.editTextPlace);


        textViewIsBusAvailable = view.findViewById(R.id.textViewIsAvailable);

        textViewIsBusAvailable.setVisibility(View.GONE);

        editTextPlace.setText(""); /** to avoid go and back ..previce text available*/

//        homeDataList = Utils.prepareData();

        recyclerView = view.findViewById(R.id.recyclerViewHome);

        homeAdapater = new HomeAdapater(view.getContext(), homeDataList);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
        recyclerView.setAdapter(homeAdapater);

        Utils.showProgressView(getActivity().getWindow(), getActivity().findViewById(R.id.progressView), true);

        getData();

        loadPlacesList(stringList);

        editTextPlace.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.d("TIME", "CharSequence : " + s + " start : " + start + " before : " + before + " count : " + count);
                singleTextAdapter.filter(s.toString());
                buses(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        editTextTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timgPicer();
            }
        });
    }

    private void loadPlacesList(List<String> stringList) {
        singleTextAdapter = new SingleTextAdapter(HomeFragment.this, getContext(), stringList);
        recyclerViewSingleText.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewSingleText.setAdapter(singleTextAdapter);
    }


    private void getData() {
        DocumentReference user = db.collection("buses").document("home");

        user.get().addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                if (task.isSuccessful()) {
                    DocumentSnapshot doc = task.getResult();
                    StringBuilder fields = new StringBuilder("");
                    fields.append("\nBUSES: ").append(doc.get("buses_tpt"));

                    List list = (List) doc.get("buses_tpt");

                    homeDataAlls.addAll(Utils.contructData(list));

                    homeDataList.clear();

                    homeDataList.addAll(Utils.prepareHomeData(homeDataAlls));
                    homeAdapater.notifyDataSetChanged();

                    homeDataList1.addAll(homeDataList);

                    stringList.addAll(Utils.prepacePlaces(homeDataAlls));

                    loadPlacesList(stringList);

                    Utils.showProgressView(getActivity().getWindow(), getActivity().findViewById(R.id.progressView), false);

                }
            }
        })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("ERROR", "" + e);
                        Utils.showProgressView(getActivity().getWindow(), getActivity().findViewById(R.id.progressView), false);

                    }
                });
    }

    private void buses(String place) {
        if (place.length() == 0) {
            textViewIsBusAvailable.setVisibility(View.GONE);
            textViewIsBusAvailable.setText("");
            homeDataList.clear();
            homeDataList.addAll(homeDataList1);
            homeAdapater.notifyDataSetChanged();

        }

    }

    @Override
    public void click(String s) {

        if (editTextTime.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Select Time", Toast.LENGTH_SHORT).show();
        } else {
            editTextPlace.setText(s);
            homeDataList.clear();
            homeDataList.addAll(Utils.filterData(homeDataAlls, selectedDate, s));

            if (homeDataList.size() == 0) {
                textViewIsBusAvailable.setVisibility(View.VISIBLE);
                textViewIsBusAvailable.setText("No bus available for " + editTextPlace.getText() + " above " + editTextTime.getText());
            } else {
                textViewIsBusAvailable.setVisibility(View.GONE);
                textViewIsBusAvailable.setText("");
            }

            homeAdapater.notifyDataSetChanged();
        }


    }

    public void timgPicer() {
        // TODO Auto-generated method stub
        Calendar mcurrentTime = Calendar.getInstance();
        final int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
//                eReminderTime.setText( selectedHour + ":" + selectedMinute);
//                editTextTime.setText(""+selectedHour+":"+selectedMinute);
                editTextTime.setText(DateAndTimeCalculation.getTime(selectedHour, selectedMinute));

                selectedDate = DateAndTimeCalculation.getDate() + " " + DateAndTimeCalculation.getTime(selectedHour, selectedMinute);
                Log.d("TIME_PICKER", "" + selectedDate);
            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

}
