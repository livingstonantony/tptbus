package com.bosco.tptbus.utils;

import android.util.Log;

import com.google.firebase.Timestamp;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class DateAndTimeCalculation {

    /**
     * get time in hh:mm am or pm
     *
     * @param hour hour
     * @param min  min
     */
    public static String getTime(int hour, int min) {

        String time = "";
        String h = "";
        String m = "";
        String zone = " AM";

        if (hour==12){
            zone = " PM";
        }
        if (hour >12) {
            hour = hour % 12;
            zone = " PM";
        }

        if (hour < 10) {
            h = "0" + hour;
        } else {
            h = "" + hour;
        }

        if (min < 10) {
            m = "0" + min;
        } else {
            m = "" + min;
        }

        time = h + ":" + m + zone;


        return time;
    }

    /**
     * Get current date
     */
    public static String getDate() {

        Date date = new Date();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd:MM:yyyy");

        String d = simpleDateFormat.format(date);

        return d;
    }

    /**
     * convert seconds to date (and take onlly time )
     */
    public static String timeStampToTime(Timestamp timestamp) {
        Date date = new Date(timestamp.getSeconds() * 1000);

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(" hh:mm aa");

        String time = simpleDateFormat.format(date);

        Log.d("SECONDS_TIME", "" + time);

        return time;
    }

    /**
     * Convert string date to Time stamp
     *
     * @param date - will have date in string format
     */
    public static Timestamp stringDateToTimestamp(String date) {
        Date date1 = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd:MM:yyyy hh:mm aa");
        try {
            date1 = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        Timestamp timestamp = new Timestamp(date1);

        return timestamp;
    }

    /**
     * Here date whatever may be.. it will convert the previce date to current date
     * time will be same
     * only previce date to current date
     */
    public static String timeStampToCurrentDate(Timestamp timestamp) {
        return DateAndTimeCalculation.getDate() + " " + DateAndTimeCalculation.timeStampToTime(timestamp);
    }

    public static long timeToSeconds(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa", Locale.getDefault());
        //        sdf.setTimeZone(TimeZone.getTimeZone("GMT")); //getting exact milliseconds at GMT
        //        sdf.setTimeZone(TimeZone.getDefault());
        Date date = null;
        try {
            date = sdf.parse(time);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime() / 1000;
    }


    public static Timestamp busReachingForBusRoute(Timestamp reach, String selectedTime) {

        Timestamp timestamp = null;

        String strReach = DateAndTimeCalculation.timeStampToCurrentDate(reach);

        Timestamp timestampReach = DateAndTimeCalculation.stringDateToTimestamp(DateAndTimeCalculation.getDate() + " 00:00 AM");
        Timestamp timestampSelected = DateAndTimeCalculation.stringDateToTimestamp(strReach);

        Timestamp sele = DateAndTimeCalculation.stringDateToTimestamp(selectedTime);

        Log.d("R", "" + timestampReach);
        Log.d("A", "" + timestampSelected);
        long ra = timestampSelected.getSeconds() - timestampReach.getSeconds();
        Log.d("R-A", "" + ra);

        long ex = sele.getSeconds() + ra;
        Log.d("ACTUAL_TIME", "" + ex);

        Date date = new Date(ex*1000);

//        timestamp = DateAndTimeCalculation.stringDateToTimestamp(date.toString());
        DateFormat sdf = new SimpleDateFormat("dd:mm:yyyy hh:mm aa");

        timestamp = DateAndTimeCalculation.stringDateToTimestamp(sdf.format(date));


        return timestamp;
    }



}
