package com.bosco.tptbus.utils;

import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.bosco.tptbus.bean.BusRouteData;
import com.bosco.tptbus.bean.HomeData;
import com.bosco.tptbus.bean.HomeDataAll;
import com.bosco.tptbus.interfaces.NameKeys;
import com.google.firebase.Timestamp;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {


    /**
     * Here i am going to take only the places
     * Avoiding dublication of the placs
     *
     * @param alls - will contain all the bus timing and places
     */
    public static List<String> prepacePlaces(List<HomeDataAll> alls) {

        List<String> list = new ArrayList<>();
        Set<String> set = new HashSet<>();

        for (HomeDataAll homeDataAll : alls) {
            for (String s : homeDataAll.getPlaceList()) {
                set.add(s);
            }
        }

        list.addAll(set);

        Collections.sort(list);

        return list;
    }

    /**
     * @param window - touch & touch
     * @param status - progress view VISIBLE & VISIBLE_GONE
     */
    public static void showProgressView(Window window, View view, boolean status) {
        if (view != null) {
            if (status) {
                window.setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE, WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                view.setVisibility(View.VISIBLE);
            } else {
                window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                view.setVisibility(View.GONE);
            }
        }

    }


    /**
     * Extracting Values
     *
     * @param list - data for all the activities in the home screen
     */
    public static List<HomeDataAll> contructData(List list) {

        List<HomeDataAll> dataAlls = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            HashMap<String, Object> hashMap = (HashMap<String, Object>) list.get(i);

            String name = (String) hashMap.get(NameKeys.HomeDataKeys.BUS_NAME_KEY);
            String URL = (String) hashMap.get(NameKeys.HomeDataKeys.IMG_URL);
            List<Timestamp> timestamps = (List<Timestamp>) hashMap.get(NameKeys.HomeDataKeys.TIME);
            List<String> placesList = (List<String>) hashMap.get(NameKeys.HomeDataKeys.PLACES);

            dataAlls.add(new HomeDataAll(name, URL, timestamps, placesList));
        }
        return dataAlls;
    }


    /**
     * Prepare data for home screen
     *
     * @param alls - data will contain needed data for the home data
     * @return homedata - data for home list view
     */
    public static List<HomeData> prepareHomeData(List<HomeDataAll> alls) {

        List<HomeData> homeData = new ArrayList<>();

        for (HomeDataAll homeDataAll : alls) {

            homeData.add(new HomeData(homeDataAll.getName(),
                    homeDataAll.getPlaceList().get(homeDataAll.getPlaceList().size() - 1),
                    homeDataAll.getDateList().get(homeDataAll.getDateList().size() - 1),
                    homeDataAll.getIMAGE_URL()
            ));

        }

        return homeData;

    }


    public static List<HomeData> filterData(List<HomeDataAll> dataPlaces, String date, String place) {

        Timestamp timestamp = DateAndTimeCalculation.stringDateToTimestamp(date);
        Log.d("TIMESTAMP_1000", "" + timestamp.getSeconds() * 1000);
        Log.d("TIMESTAMP", "" + timestamp.getSeconds());

        long l = DateAndTimeCalculation.timeToSeconds(DateAndTimeCalculation.timeStampToTime(timestamp));

        Log.d("TIME_SEC", "" + l);

        List<HomeData> data = new ArrayList<>();

        String p = "";
        for (HomeDataAll places : dataPlaces) {
            int i = 0;
            for (String s : places.getPlaceList()) {

                if ((s.equalsIgnoreCase(place))) {
                    p = s;
                    if (i == 0) {

                        int j = 0;
                        for (Timestamp time : places.getDateList()) {

                            String s1 = DateAndTimeCalculation.timeStampToCurrentDate(time);

                            Log.d("CURRENT_DATE", s1);

                            Timestamp ct = DateAndTimeCalculation.stringDateToTimestamp(s1);

                            if (ct.getSeconds() > timestamp.getSeconds()) {
                                if (j == 0) {
                                    data.add(new HomeData(places.getName(), p, ct, places.getIMAGE_URL()));
                                    j++;
                                }
                            }
                        }

                    }
                    i++;
                }
            }

        }

        return data;
    }


    /**
     * Prepare dummy data for the bus route data
     */
    public static List<BusRouteData> prepareDummnyRouteData() {
        List<BusRouteData> busRouteData = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
//            busRouteData.add(new BusRouteData("test"+i,i,"11:00 AM"));
        }
        return busRouteData;
    }

    public static List<BusRouteData> preateBusRouteData(List list, String selectedTime, String destination) {

        List<BusRouteData> busRouteData = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {

            HashMap<String, Object> hashMap = (HashMap<String, Object>) list.get(i);

            String place = (String) hashMap.get(NameKeys.BusRouteKesy.PLACE);
            String price = (String) hashMap.get(NameKeys.BusRouteKesy.PRICE);
            Timestamp timestamp = (Timestamp) hashMap.get(NameKeys.BusRouteKesy.TIME);

            Timestamp timestampReahingTime = DateAndTimeCalculation.busReachingForBusRoute(timestamp, selectedTime);

            busRouteData.add(new BusRouteData(place, Integer.parseInt(price), timestampReahingTime));

            if (destination.equalsIgnoreCase(place)) {
                break;
            }

        }
        return busRouteData;

    }

    /**
     * Returns the consumer friendly device name
     */
    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        }
        return capitalize(manufacturer) + " " + model;
    }

    /**
     * It's belongs to @method getDeviceName
     */
    private static String capitalize(String str) {
        if (TextUtils.isEmpty(str)) {
            return str;
        }
        char[] arr = str.toCharArray();
        boolean capitalizeNext = true;

        StringBuilder phrase = new StringBuilder();
        for (char c : arr) {
            if (capitalizeNext && Character.isLetter(c)) {
                phrase.append(Character.toUpperCase(c));
                capitalizeNext = false;
                continue;
            } else if (Character.isWhitespace(c)) {
                capitalizeNext = true;
            }
            phrase.append(c);
        }

        return phrase.toString();
    }


}
